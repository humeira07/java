package com.example.demo;

import static org.hamcrest.Matchers.containsString; import static
		org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get; import static
		org.springframework.test.web.servlet.result.MockMvcResultMatchers.content; import static
		org.springframework.test.web.servlet.result.MockMvcResultMatchers.status; import static
		org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Test
	public void contextLoads() {
	}

}

